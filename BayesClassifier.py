import pandas as pd
from collections import defaultdict as dd
import numpy as np
from copy import deepcopy

filname = 'Data/train_test.csv'
evaluation_filename = 'Data/evaluate.csv'

class BayesClassifier():
    def __init__(self, train_df):
        self.poems = train_df


        
        
    def count_poems_distribution(self):
        saadi_df = self.poems.loc[self.poems.label == 'saadi']
        hafez_df = self.poems.loc[self.poems.label == 'hafez']
        self.poet_df = pd.DataFrame ({'name': ['saadi', 'hafez'], 'count': [saadi_df.shape[0], hafez_df.shape[0]]})
        probabilities = [self.poet_df['count'][0] / self.poet_df.sum()['count'], \
                         self.poet_df['count'][1] / self.poet_df.sum()['count']]
        self.poet_df['probability'] = probabilities

    
    def _train(self, poem):
        # print(poem)

        words = set(poem.text.split(' '))
        poet = poem.label
        col_name = poet + '_count'
        len_poem = len(poem.text.split(' '))
        try:
            self.num_words.loc[len_poem].at[col_name] += 1
        except KeyError:
            self.num_words = self.num_words.append(pd.DataFrame([[0]*len(list(self.num_words.columns))],\
                    columns = list(self.num_words.columns), index=[len_poem]))
                
            self.num_words.loc[len_poem].at[col_name] += 1
            
        for word in words:
                try:
                    self.words.loc[word].at[col_name] += 1
                    # print('repeated word', word)
                except KeyError:
                    # print('new word', word)                    
                    self.words = self.words.append(pd.DataFrame([[0]*len(list(self.words.columns))],\
                         columns = list(self.words.columns), index=[word]))
                    
                    self.words.loc[word].at[col_name] += 1
            
        
                
    def train(self):
        self.count_poems_distribution()
        self.words = pd.DataFrame(columns=['hafez_count', 'saadi_count'])
        self.num_words = pd.DataFrame(columns=['hafez_count', 'saadi_count'])
        self.hafez_poems = self.poems.loc[self.poems.label == 'hafez']
        self.saadi_poems = self.poems.loc[self.poems.label == 'saadi']
        self.poems.apply(lambda poem: self._train(poem), axis=1)
        # for row in self.poems:
        #     self._train(row)
        # print(self.words)
        # print(self.num_words)
        
        
    def predict(self, poem):
        poem_length = len(poem.split(' '))        
        poem = set(poem.split(' '))
        prob = {'saadi': self.poet_df['probability'][0], 'hafez': self.poet_df['probability'][1]} 
        try:
            prob['saadi'] *= self.num_words.loc[poem_length].at['saadi_count'] / len(self.saadi_poems)
            prob['hafez'] *= self.num_words.loc[poem_length].at['hafez_count'] / len(self.hafez_poems)
        except KeyError:
            pass
            
        for word in poem:
            try:
                prob['saadi'] *= (self.words.loc[word].at['saadi_count'] / len(self.saadi_poems))                
                prob['hafez'] *= (self.words.loc[word].at['hafez_count'] / len(self.hafez_poems))
                                  
            except KeyError:
                continue
            
        return 'hafez' if prob['hafez'] > prob['saadi'] else 'saadi' 
    
    def laplas(self):
        for word in self.words.index:
            self.words.loc[word].at['hafez_count'] += 1
            self.words.loc[word].at['saadi_count'] += 1 

    
        for num in self.num_words.index:
            self.num_words.loc[num].at['hafez_count'] += 1
            self.num_words.loc[num].at['saadi_count'] += 1  

               
    def _test(self, poem):
        real_poet =  poem.label
        poet = self.predict(poem.text)
        # print(poet)
        self.detected[poet] += 1  
        if poet == real_poet:
            self.correct[poet] += 1
        
    def test(self, test_df):    
        self.correct = {'saadi': 0, 'hafez': 0}
        self.detected = {'saadi': 0, 'hafez': 0}
        
        count = len(test_df)
        test_df.apply(lambda row: self._test(row), axis=1)
        # print(self.correct, self.detected)
                
        return {'recall_hafez': self.correct['hafez']/len(test_df[test_df.label == 'hafez']), \
            'recall_saadi': self.correct['saadi']/ len(test_df.loc[test_df.label == 'saadi']), \
            'precision_hafez': self.correct['hafez']/ self.detected['hafez'], \
            'precision_saadi': self.correct['saadi']/ self.detected['saadi'],\
            'accuracy': (self.correct['hafez'] + self.correct['saadi']) / count}
   
    def _evaluate(self, row):
        # print(row)
        # print('eval', self.eval.index)
        self.eval.loc[self.eval.index == row.id - 1, 'label']= self.predict(row.text)
        # print(row, row.index[])
        
    def evaluate(self, eval_df):
        self.eval = deepcopy(eval_df)
        self.eval['label'] = ['ananymous'] * len(eval_df)
        self.eval.apply(lambda row: self._evaluate(row), axis=1)
        return self.eval
        
        
from time import time

if __name__ == '__main__':
    df = pd.read_csv('Data/train_test.csv')
    results = []
    # for i in range(5):
    #     tic = time()
    #     df = df.sample(frac=1)
    #     train , test = np.split(df, [int(0.8* len(df))])
    #     classifier = BayesClassifier(train)
    #     classifier.train()
    #     classifier.laplas()
    #     results.append(classifier.test(test))
    #     toc = time()
    #     print(results[-1])
    #     print(toc - tic)
    
    # print(results)
    
    evaluation_df = pd.read_csv(evaluation_filename)
    evaluator = BayesClassifier(df)
    evaluator.train()
    evaluation = evaluator.evaluate(evaluation_df)
    evaluation = evaluation.drop(columns = 'id')
    evaluation.to_csv('evaluation.csv', index = False)
    hafez_len = len(evaluation.loc[evaluation.label == 'hafez'])
    print(hafez_len, len(evaluation))
    
    



        
