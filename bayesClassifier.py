import pandas as pd
from collections import defaultdict as dd
import numpy as np

filname = 'Data/train_test.csv'

class BayesClassifire():
    def __init__(self, dataframe):
        self.poems = dataframe
        # print(poems)
        self.words = pd.DataFrame(columns=['hafez_count', 'saadi_count'])
        self.num_words = pd.DataFrame(columns=['hafez_count', 'saadi_count'])
        
        
    def count_poems_distribution(self):
        saadi_df = self.poems.loc[self.poems.label == 'saadi']
        hafez_df = self.poems.loc[self.poems.label == 'hafez']
        # print(saadi_df)
        self.poet_df = pd.DataFrame ({'name': ['saadi', 'hafez'], 'count': [saadi_df.shape[0], hafez_df.shape[0]]})
        probabilities = [self.poet_df['count'][0] / self.poet_df.sum()['count'], \
                         self.poet_df['count'][1] / self.poet_df.sum()['count']]
        self.poet_df['probability'] = probabilities
        # print(self.poet_df)


    def words_cond_distr(self, poet, poems, words):
        print('words conditional distribution for ' + poet)
        col_name = poet + '_count'
        for poem in poems['text']:
            words_list = set(poem.split(' '))
            for word in words_list:
                try:
                    words.loc[word].at[col_name] += 1
                except KeyError:
                    words = words.append(pd.DataFrame([[0]*len(list(words.columns))],\
                         columns = list(words.columns), index=[word]))
                    
                    words.loc[word].at[col_name] += 1
        print(words)
        return words 
    

        
    
    def num_words_disr(self,poet, poems, num_words):   
        print('poem length conditional distribution for ' + poet)        
        col_name = poet + '_count'
        print(num_words)
        for poem in poems['text']:
            words = poem.split(' ')
            word_length = len(words)
            try:
                num_words.loc[len(words)].at[col_name] = num_words.loc[len(words)].at[col_name] + 1             
            except KeyError:
                # print(len(words))
                num_words = num_words.append(pd.DataFrame([[0]*len(list(num_words.columns))],\
                        columns = list(num_words.columns), index=[len(words)]))
                
                num_words.loc[len(words)].at[col_name] += 1
        # print('saadi', num_words)
        return num_words
                
    def train(self):
        print('Classifying')
        saadi_df = self.poems.loc[self.poems.label == 'saadi']
        hafez_df = self.poems.loc[self.poems.label == 'hafez']
        self.count_poems_distribution()        
        self.words = self.words_cond_distr('hafez', hafez_df, self.words)
        self.words = self.words_cond_distr('saadi', saadi_df, self.words)
        self.num_words = self.num_words_disr('hafez', hafez_df, self.num_words)
        self.num_words =self.num_words_disr('saadi', saadi_df, self.num_words)
        print(self.words)
        print(self.num_words)
        self.word_saadi_sum = self.words.sum()['saadi_count']
        self.word_hafez_sum = self.words.sum()['hafez_count']
        self.word_num_saadi_sum = self.words.sum()['saadi_count']
        self.word_num_hafez_sum = self.words.sum()['hafez_count']
        self.saadi_count = len(self.poems.loc[self.poems.label == 'saadi'])
        self.hafez_count = len(self.poems.loc[self.poems.label == 'hafez'])
        
        
        
    def guess_poet(self, poem):
        poem = poem.split()
        prob = {'saadi': self.poet_df['probability'][0], 'hafez': self.poet_df['probability'][1]} 
        try:
            prob['saadi'] *= self.num_words.loc[len(poem)].at['saadi_count'] / self.saadi_count
            prob['hafez'] *= self.num_words.loc[len(poem)].at['hafez_count'] / self.hafez_count
        except KeyError:
            pass
        
        for word in poem:
            try:
                prob['saadi'] *=(self.words.loc[word].at['saadi_count'] / self.saadi_count)
                prob['hafez'] *= (self.words.loc[word].at['hafez_count'] /self.hafez_count)
            except KeyError:
                continue
            
        return 'hafez' if prob['hafez'] > prob['saadi'] else 'saadi' 
    
    def laplas(self):
        if 0 in self.words['hafez_count']:
            for word in self.words.index:
                self.words.loc[word].at['hafez_count'] += 1
        
        if 0 in self.words['saadi_count']:
            for word in self.words.index:
                self.words.loc[word].at['saadi_count'] += 1 
        
        if 0 in self.num_words['hafez_count']:
            for num in self.num_words.index:
                self.num_words.loc[num].at['hafez_count'] += 1
                
        if 0 in self.num_words['saadi_count']:
            for num in self.num_words.index:
                self.num_words.loc[num].at['saadi_count'] += 1  
                    
        self.word_saadi_sum = self.words.sum()['saadi_count']
        self.word_hafez_sum = self.words.sum()['hafez_count']
        self.word_num_saadi_sum = self.words.sum()['saadi_count']
        self.word_num_hafez_sum = self.words.sum()['hafez_count']
               
        
    def test(self, test_df):    
        correct = {'saadi': 0, 'hafez': 0}
        detected = {'saadi': 0, 'hafez': 0}
        
        count = 0
        print('start_test')
        for index, poem in test_df.iterrows():
            count += 1
            print(count)
            poet = self.guess_poet(poem['text'])
            detected[poet] += 1

            if poet == poem['label']:
                correct[poet] += 1
                
        return {'recall_hafez': correct['hafez']/len(test_df[test_df.label == 'hafez']), \
            'recall_saadi': correct['saadi']/ len(test_df.loc[test_df.label == 'saadi']), \
            'precision_hafez': correct['hafez']/ detected['hafez'], \
            'precision_saadi': correct['saadi']/ detected['saadi'],\
            'accuracy': (correct['hafez'] + correct['saadi']) / count}

            
        


from time import time

if __name__ == '__main__':
    df = pd.read_csv('Data/train_test.csv')
    results = []
    for i in range(5):
        tic = time()
        df = df.sample(frac=1)
        train , test = np.split(df, [int(0.80* len(df))])
        classifier = BayesClassifire(train)
        classifier.train()
        classifier.laplas()
        results.append(classifier.test(test))
        toc = time()
        print(results[-1])
        print(toc - tic)
    
    print(results)
        
    
    # poem = input('Enter poem')
