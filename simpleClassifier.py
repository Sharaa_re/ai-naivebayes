from bayesClassifier1 import BayesClassifier
import pandas as pd
import numpy as np

from time import time

if __name__ == '__main__':
    df = pd.read_csv('Data/train_test.csv')
    results = []
    for i in range(5):
        tic = time()
        train , test = np.split(df, [int(0.80* len(df))])
        classifier = BayesClassifier(train)
        classifier.train()
        # classifier.laplas()
        results.append(classifier.test(test))
        toc = time()
        print(results[-1])
        print(toc - tic)
    
    print(results)
    